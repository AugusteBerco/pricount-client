import { ApolloProvider } from "@apollo/client";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import {
  ProvideUser,
  useCurrentUser,
  UserProvider,
} from "hooks/useCurrentUser";
import { AuthProvider, ProvideAuth, useAuth } from "hooks/user-auth";
import { apolloClient } from "lib/apolloClient";
import React from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  RouteProps,
  Switch,
  useLocation,
} from "react-router-dom";
import LoadingLayout from "ui/layouts/LoadingLayout";
import ExpenseLists from "ui/pages/ExpenseLists";
import HomePage from "ui/pages/HomePage";
import LoginPage from "ui/pages/Login";
import RegisterPage from "ui/pages/Register";

const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#152b49",
      light: "#415375",
      dark: "#000022",
    },
    secondary: {
      main: "#fe8263",
      light: "#ffb391",
      dark: "#c65238",
    },
    background: {
      paper: "#415375",
      default: "#263548",
    },
  },
});

const ProtectedRoute = ({ component: Component, path }: RouteProps) => {
  const { isLoggedIn, isLoggingIn } = useCurrentUser() as UserProvider;

  const location = useLocation();

  console.log({ isLoggedIn, isLoggingIn });

  if (isLoggingIn) return <LoadingLayout />;

  return isLoggedIn ? (
    <Route component={Component} path={path} />
  ) : (
    <Redirect
      to={{
        pathname: "/login",
        state: { from: location },
      }}
    />
  );
};

const AnonRoute = ({ component: Component, path }: RouteProps) => {
  const auth = useAuth() as AuthProvider;
  const { isLoggedIn, isLoggingIn } = auth;
  const location = useLocation();

  if (isLoggingIn) return <LoadingLayout />;
  return <Route component={Component} path={path} />;

  // return !isLoggedIn ? (
  //   <Route component={Component} path={path} />
  // ) : (
  //   <Redirect
  //     to={{
  //       pathname: "/",
  //       state: { from: location },
  //     }}
  //   />
  // );
};

function App() {
  return (
    <ThemeProvider theme={theme}>
      <ProvideAuth>
        <Router>
          <Switch>
            <AnonRoute path="/login" component={LoginPage}></AnonRoute>
            <AnonRoute path="/register" component={RegisterPage}></AnonRoute>
            <ApolloProvider client={apolloClient}>
              <ProvideUser>
                <ProtectedRoute path="/expenses" component={ExpenseLists} />
              </ProvideUser>
            </ApolloProvider>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </Router>
      </ProvideAuth>
    </ThemeProvider>
  );
}

export default App;
