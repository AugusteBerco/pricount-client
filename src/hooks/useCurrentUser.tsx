import { GetCurrentUserQuery, useGetCurrentUserQuery } from "generated-graphql";
import { createContext, useContext, useEffect, useState } from "react";

export interface UserProvider {
  user?: any;
  isLoggedIn: boolean;
  isLoggingIn: boolean;
}

const UserContext = createContext<UserProvider | undefined>(undefined);

export const ProvideUser: React.FC = ({ children }) => {
  const userContext = useProvideUser();
  return (
    <UserContext.Provider value={userContext}>{children}</UserContext.Provider>
  );
};

export const useCurrentUser = () => {
  return useContext(UserContext);
};

export const useProvideUser = () => {
  const { data, loading } = useGetCurrentUserQuery();
  const [user, setUser] = useState<
    GetCurrentUserQuery["currentUser"] | undefined
  >();

  useEffect(() => {
    if (!loading && !user && data) {
      setUser(data.currentUser);
    }
    // return () => null;
  }, [data, loading, user]);

  return {
    user: data?.currentUser,
    isLoggingIn: loading,
    isLoggedIn: !!user || !!data?.currentUser,
  };
};
