import axios from "lib/axios";
import React, { createContext, useContext, useState } from "react";
import { RegisterFormInputs } from "typings";

export interface AuthProvider {
  token?: string | null;
  user?: any;
  isLoggedIn: boolean;
  isLoggingIn: boolean;
  signin: (username: string, password: string) => Promise<void>;
  signup: (registerInput: RegisterFormInputs) => Promise<void>;
  signout: () => void;
}
var AuthContext = createContext<AuthProvider | undefined>(undefined);

// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export const ProvideAuth: React.FC<{}> = ({ children }) => {
  const auth = useProvideAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};

// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = () => {
  return useContext(AuthContext);
};

// Provider hook that creates auth object and handles state
function useProvideAuth() {
  const storedToken = localStorage.getItem("token");

  const [token, setToken] = useState(storedToken);
  const [isLoggingIn, setIsLoggingIn] = useState(false);

  // Wrap any Firebase methods we want to use making sure ...
  // ... to save the user to state.
  const signin = (username: string, password: string) => {
    setIsLoggingIn(true);
    return axios
      .post("/auth/login", { username, password }, { withCredentials: true })
      .then((res) => {
        setToken(res.data.access_token);
        localStorage.setItem("token", res.data.access_token);
        setIsLoggingIn(false);
        // setUser(res.data.user);
      });
  };

  const signup = (registerInput: RegisterFormInputs) => {
    setIsLoggingIn(true);
    return axios
      .post("/auth/register", registerInput, { withCredentials: true })
      .then((res) => {
        setIsLoggingIn(false);
        setToken(res.data.access_token);
        localStorage.setItem("token", res.data.access_token);
        // setUser(res.data.user);
      });
  };

  const signout = () => {
    return axios.post("/auth/logout", { withCredentials: true }).then((res) => {
      setToken(null);
      localStorage.setItem("token", "");
      // setUser(undefined);
    });
  };

  //   const sendPasswordResetEmail = (email) => {
  //     return firebase
  //       .auth()
  //       .sendPasswordResetEmail(email)
  //       .then(() => {
  //         return true;
  //       });
  //   };

  //   const confirmPasswordReset = (code, password) => {
  //     return firebase
  //       .auth()
  //       .confirmPasswordReset(code, password)
  //       .then(() => {
  //         return true;
  //       });
  //   };

  // Subscribe to user on mount
  // Because this sets state in the callback it will cause any ...
  // ... component that utilizes this hook to re-render with the ...
  // ... latest auth object.
  //   useEffect(() => {
  //     const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
  //       if (user) {
  //         setUser(user);
  //       } else {
  //         setUser(false);
  //       }
  //     });

  //     // Cleanup subscription on unmount
  //     return () => unsubscribe();
  //   }, []);

  // Return the user object and auth methods
  return {
    // user,
    token,
    signin,
    signup,
    signout,
    isLoggedIn: token !== undefined && token != null,
    isLoggingIn,
  };
}
