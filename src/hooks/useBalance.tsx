import {
  ExpenseFieldsFragment,
  ParticipantInfoFragment,
} from "generated-graphql";
import currency from "currency.js";
import { Balance } from "typings";

export const useBalance = (
  expenses?: ExpenseFieldsFragment[] | null,
  participants?: ParticipantInfoFragment[] | null
) => {
  if (!expenses || !participants) {
    return [];
  }
  //   let balances: Balance[] = ;

  const balances = expenses.reduce(
    (balances: Balance[], expense) => {
      return balances.map((balance) => {
        let total;
        if (balance.participant.id === expense.paidBy.id) {
          total = expense.forWhom
            .map((fw) => fw.id)
            .includes(balance.participant.id)
            ? balance.total
                .add(currency(expense.amount))
                .subtract(
                  currency(expense.amount, { symbol: "€" }).divide(
                    expense.forWhom.length
                  )
                )
            : balance.total.add(currency(expense.amount));
        } else {
          total = expense.forWhom
            .map((fw) => fw.id)
            .includes(balance.participant.id)
            ? balance.total.subtract(
                currency(expense.amount, { symbol: "€" }).divide(
                  expense.forWhom.length
                )
              )
            : balance.total;
        }

        return {
          participant: balance.participant,
          total,
        };
      });
    },
    participants.map((p) => ({
      participant: p,
      total: currency(0, { symbol: "€" }),
    }))
  );
  const sortcurrencies: (a: Balance, b: Balance) => number = (a, b) =>
    a.total.value - b.total.value;

  return Object.values(balances).sort(sortcurrencies);
};

export const useTransfers = (balances: Balance[]) => {
  const balancesSum = balances.reduce((sum: Balance[], b: Balance) => {
    if (b.total.value !== 0) sum.push(b);
    return sum;
  }, []);

  if (!balancesSum.length) {
    return [];
  }

  const transfers = [];
  let b = [...balances.map((balance) => ({ ...balance }))];
  while (b.length !== 0) {
    const sortedBalances = b.sort(
      (a: Balance, b: Balance) => a.total.value - b.total.value
    );

    const mostOwns = sortedBalances[0];

    let closestBalance = mostOwns.total.value;
    const suitableTransfer = sortedBalances.reduce((res, balance) => {
      if (
        Math.abs(mostOwns.total.value + balance.total.value) <=
        Math.abs(closestBalance)
      ) {
        closestBalance = res.total.value + balance.total.value;

        return balance;
      }
      return res;
    }, mostOwns);
    transfers.push({
      name: mostOwns.participant.name,
      to: suitableTransfer.participant.name,
      amount:
        suitableTransfer.total.value >= Math.abs(mostOwns.total.value)
          ? currency(Math.abs(mostOwns.total.value), { symbol: "€" })
          : currency(suitableTransfer.total.value, { symbol: "€" }),
    });

    const mostOwnsCopy = currency(mostOwns.total.value, { symbol: "€" });

    sortedBalances[0].total =
      suitableTransfer.total.value >= Math.abs(mostOwns.total.value)
        ? currency(0, { symbol: "€" })
        : mostOwns.total.add(suitableTransfer.total);

    suitableTransfer.total =
      suitableTransfer.total.value > Math.abs(mostOwnsCopy.value)
        ? suitableTransfer.total.add(mostOwnsCopy)
        : currency(0, { symbol: "€" });

    b = sortedBalances.filter((b) => b.total.value !== 0);
  }
  return transfers;
};
