import currency from "currency.js";
import { ParticipantInfoFragment } from "generated-graphql";

export type Participant = {
  _id: string;
  id?: string;
  name: string;
  userId: string;
};

export type Balance = {
  participant: ParticipantInfoFragment;
  total: currency;
};

export type Transfer = {
  name: string;
  to: string;
  amount: currency;
};

export type Expense = {
  _id: string;
  listId: string;
  title: string;
  description: string;
  expenseType: string;
  category: string;
  amount: currency | number;
  paidBy: string;
  forWhom: string[];
  date: Date;
  previous: () => string | undefined;
  next: () => string | undefined;
};

export type ExpenseInput = {
  listId: string;
  title: string;
  expenseType: string;
  category: string;
  amount: currency | number;
  paidBy: string;
  forWhom: string[];
  date: Date;
};

export type ExpenseList = {
  _id: string;
  title: string;
  expenses: () => Expense[];
  participants: () => Participant[];
  totalExpenses: () => currency;
  totalFor: (userId: string) => currency;
  balances: () => Balance[];
  transfers: () => Transfer[];
};

export type ListFormInputs = {
  title: string;
  description: string;
  ownerName: string;
  others?: Array<{ id?: number; _id?: string; name: string }>;
};

export interface RegisterFormInputs {
  username: string;
  email: string;
  password: string;
  repeat_password: string;
}

export type User = {
  id: number;
  username: string;
  email: string;
};
