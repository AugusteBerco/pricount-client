import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import { AuthProvider, useAuth } from "hooks/user-auth";
import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
// import useAccount from "/imports/hooks/useAccount";
import UserMenu from "../components/UserMenu";
import MasterLayout from "./MasterLayout";

const Layout: React.FC<{}> = ({ children }) => {
  const auth = useAuth() as AuthProvider;

  return (
    <MasterLayout
      appBarContent={
        <Toolbar style={{ justifyContent: "space-between" }} disableGutters>
          {/* <Typography variant="h6" className={classes.title}>
            MoneyTrack
          </Typography> */}
          <Link to="/" style={{ flexGrow: 1 }}>
            <img
              src="/pricount logo bw.svg"
              // width="300"
              height="60"
              alt="Pricount logo"
              style={{ marginLeft: 8 }}
            />
          </Link>

          {auth.isLoggedIn ? (
            <UserMenu />
          ) : (
            <>
              <Button color="inherit" component={Link} to="/login">
                Login
              </Button>
              <Button color="inherit" component={Link} to="/register">
                Register
              </Button>
            </>
          )}
        </Toolbar>
      }
    >
      <>{children}</>
    </MasterLayout>
  );
};
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
