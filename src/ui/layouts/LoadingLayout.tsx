import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import React from "react";
import Loading from "ui/components/Loading";
import MasterLayout from "./MasterLayout";

const LoadingLayout = () => (
  <MasterLayout
    appBarContent={
      <Toolbar>
        <Typography variant="h6">MoneyTrack</Typography>
      </Toolbar>
    }
  >
    <Loading />
  </MasterLayout>
);

export default LoadingLayout;
