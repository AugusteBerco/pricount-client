import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import CheckIcon from "@material-ui/icons/Check";
import React from "react";
import { useHistory } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import MasterLayout from "./MasterLayout";

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBarSpacer: theme.mixins.toolbar,
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
}));

interface IProps {
  title: string;
  handleCheck: () => void;
}
const FormLayout: React.FC<IProps> = ({ title, handleCheck, children }) => {
  const classes = useStyles();
  const history = useHistory();

  const handleBack = () => history.goBack();

  return (
    <MasterLayout
      appBarContent={
        <Toolbar>
          <IconButton onClick={handleBack} color="inherit">
            {" "}
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {title}
          </Typography>
          <IconButton onClick={handleCheck} color="inherit">
            <CheckIcon />
          </IconButton>
        </Toolbar>
      }
    >
      {/* <div className={classes.appBarSpacer} /> */}

      <Container>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <>{children}</>
          </Grid>
        </Grid>
      </Container>
    </MasterLayout>
  );
};

export default FormLayout;
