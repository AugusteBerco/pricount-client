import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";

const Loading = () => (
  <Box
    style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexGrow: 1,
      // minHeight: "100vh",
    }}
  >
    <CircularProgress />
  </Box>
);

export default Loading;
