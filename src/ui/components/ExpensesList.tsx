import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import { makeStyles } from "@material-ui/core/styles";
import {
  GetPricountByIdQuery,
  ParticipantInfoFragment,
  ExpenseFieldsFragment,
} from "generated-graphql";
import React from "react";
import ExpenseListItem from "ui/components/ExpenseListItem";

const useStyles = makeStyles(() => ({
  listContainer: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
  },
}));

interface IProps {
  expenseList: GetPricountByIdQuery["getPricount"];
  expenses?: ExpenseFieldsFragment[] | null;
  participants: ParticipantInfoFragment[];
}

const ExpensesList: React.FC<IProps> = ({ expenses, participants }) => {
  const classes = useStyles();

  return (
    <div className={classes.listContainer}>
      {expenses && expenses.length ? (
        <List style={{ width: "100%" }}>
          {expenses.map((expense) => (
            <ExpenseListItem
              key={expense.id}
              expense={expense}
              participants={participants}
            />
          ))}
        </List>
      ) : (
        <Container maxWidth="sm" style={{ flexGrow: 1 }}>
          <Grid container>
            <Grid item xs={12}>
              <p>No expenses yet</p>
            </Grid>
          </Grid>
        </Container>
      )}
      {/* </Grid>
        </Grid>
      </Container> */}
    </div>
  );
};

export default ExpensesList;
