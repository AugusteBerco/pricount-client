import { yupResolver } from "@hookform/resolvers/yup";
import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";
import { format } from "date-fns";
import FormLabel from "@material-ui/core/FormLabel";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import NumberFormat from "react-number-format";
import * as yup from "yup";
import { EXPENSE_CATEGORIES, EXPENSE_TYPES } from "lib/constants";
import { ExpenseInput } from "typings";
import FormLayout from "ui/layouts/FormLayout";
import {
  ExpenseCategories,
  ExpenseFieldsFragment,
  ExpenseTypes,
  GetPricountByIdQuery,
} from "generated-graphql";
import { useCurrentUser, UserProvider } from "hooks/useCurrentUser";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
  },
}));

const ExpenseSchema = yup.object().shape({
  title: yup.string().required(),
  expenseType: yup.string(),
  category: yup.string(),
  amount: yup.number().required(),
});

interface IProps {
  participants: GetPricountByIdQuery["getPricount"]["participants"];
  expense?: ExpenseFieldsFragment;
  saveExpense: (expense: ExpenseInput) => void;
}

const ExpenseForm: React.FC<IProps> = ({
  expense,
  participants,
  saveExpense,
}) => {
  const classes = useStyles();
  const { user } = useCurrentUser() as UserProvider;

  const { register, handleSubmit, control, errors } = useForm({
    defaultValues: {
      title: expense ? expense.title : "",
      expenseType: expense ? expense.type : ExpenseTypes.Expense,
      category: expense ? expense.category : ExpenseCategories.Unset,
      date: expense
        ? format(new Date(expense.doneAt), "yyyy-MM-dd")
        : format(new Date(), "yyyy-MM-dd"),
      amount: expense ? expense.amount : 0,
      paidBy: expense
        ? expense.paidBy.id
        : participants.find((p) => p.user?.id === user?.id)?.id,
      forWhom: expense
        ? expense.forWhom.reduce((acc: any, pId) => {
            acc[`${pId.id}`] = true;
            return acc;
          }, {})
        : participants.reduce((acc: any, p) => {
            acc[`${p.id}`] = true;
            return acc;
          }, {}),
    },
    resolver: yupResolver(ExpenseSchema),
  });

  return (
    <FormLayout
      title={expense ? "Edit Expense" : "New Expense"}
      handleCheck={handleSubmit(saveExpense)}
    >
      <Container className={classes.root}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              fullWidth
              name="title"
              inputRef={register}
              label="Title"
              error={!!errors.title}
              helperText={errors.title?.message}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="expenseType"
              control={control}
              render={({ value, onChange }) => (
                <TextField
                  id="type-input"
                  select
                  label="Type"
                  value={value}
                  onChange={onChange}
                >
                  {EXPENSE_TYPES.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="category"
              control={control}
              render={({ value, onChange }) => (
                <TextField
                  id="category-input"
                  select
                  label="Category"
                  value={value}
                  onChange={onChange}
                >
                  {EXPENSE_CATEGORIES.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="amount"
              control={control}
              defaultValue={expense ? expense.amount : null}
              render={({ value, onChange }) => (
                <NumberFormat
                  label="Amount"
                  inputMode="numeric"
                  value={value}
                  onValueChange={(v) => onChange(v.floatValue)}
                  customInput={TextField}
                  thousandSeparator
                  // isNumericString
                  suffix="€"
                  decimalScale={2}
                  fixedDecimalScale
                  error={!!errors.amount}
                  helperText={errors.amount?.message}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="date"
              name="date"
              label="Date"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
              inputRef={register}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="paidBy"
              control={control}
              render={({ value, onChange }) => (
                <TextField
                  id="paid-input"
                  select
                  value={value}
                  label="Paid by"
                  onChange={onChange}
                  fullWidth
                >
                  {participants.map((participant) => (
                    <MenuItem key={participant.id} value={participant.id}>
                      {participant.name}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <FormControl component="fieldset">
              <FormLabel component="legend">FOR WHOM</FormLabel>
              <FormGroup>
                {participants.map((participant) => (
                  <FormControlLabel
                    key={participant.id}
                    control={
                      <Controller
                        name={`forWhom[${participant.id}]`}
                        control={control}
                        defaultValue={false}
                        render={({ onChange, onBlur, value, ref }) => (
                          <Checkbox
                            onBlur={onBlur}
                            onChange={(e) => {
                              onChange(e.target.checked);
                            }}
                            checked={value}
                            inputRef={ref}
                          />
                        )}
                      />
                    }
                    label={participant.name}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Grid>
        </Grid>
      </Container>
    </FormLayout>
  );
};

export default ExpenseForm;
