import React, { Fragment } from "react";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import { GetCurrentUserQuery } from "generated-graphql";
import InputAdornment from "@material-ui/core/InputAdornment";

interface IProps {
  add: (index: number) => void;
  remove: (index: number) => void;
  participant: {
    _id?: string;
    id?: number | string;
    name?: string;
    user?: number;
  };
  index: number;
  register: () => any;
  canRemove: boolean;
  user: GetCurrentUserQuery["currentUser"];
}

const ParticipantInput: React.FC<IProps> = ({
  add,
  remove,
  participant,
  index,
  register,
  canRemove,
  user,
}) => {
  return (
    <Fragment>
      <Grid item xs={9}>
        <input
          type="hidden"
          name={`participants[${index}].id`}
          defaultValue={participant.id}
          ref={register()}
        />
        <TextField
          name={`participants[${index}].name`}
          inputRef={register()}
          // variant="outlined"
          label="Name"
          fullWidth
          InputProps={{
            endAdornment:
              participant.user === user.id ? (
                <InputAdornment position="end">(me)</InputAdornment>
              ) : null,
          }}
          defaultValue={participant.name}
        />
      </Grid>
      <Grid item xs={3}>
        <IconButton onClick={() => add(index)}>
          <AddIcon />
        </IconButton>
        {canRemove ? (
          <IconButton onClick={() => remove(index)}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </Grid>
    </Fragment>
  );
};

export default ParticipantInput;
