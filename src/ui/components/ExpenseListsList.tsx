import Container from "@material-ui/core/Container";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core/styles";
import { useGetUserPricountsQuery } from "generated-graphql";
import { useHistory, useRouteMatch } from "react-router-dom";
import Loading from "ui/components/Loading";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

const ExpensesListsList = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const classes = useStyles();

  const { data, loading } = useGetUserPricountsQuery();

  console.log({ data, loading });

  const handleListClick = (listId: number) => {
    history.push(`${match.url}/${listId}`);
  };

  if (loading) {
    return <Loading />;
  }

  const expenses = data?.getPricountsForUser;

  if (expenses && expenses.length) {
    return (
      <div className={classes.root}>
        <List>
          {expenses.map((expense) => (
            <ListItem
              key={expense.id}
              onClick={() => handleListClick(expense.id)}
              button
            >
              <ListItemText
                primary={expense.title}
                secondary={
                  expense.description
                    ? expense.description.trim()
                    : "No description"
                }
              />
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
  return (
    <Container>
      <p>No pricounts yet</p>
    </Container>
  );
};

export default ExpensesListsList;
