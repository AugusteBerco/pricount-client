import React, { useState } from "react";
import { Menu, MenuItem } from "@material-ui/core";
import MoreVert from "@material-ui/icons/MoreVert";
import IconButton from "@material-ui/core/IconButton";
import { useAuth, AuthProvider } from "hooks/user-auth";
// import { Meteor } from "meteor/meteor";

const UserMenu = () => {
  const auth = useAuth() as AuthProvider;
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const handleMenuClick: (e: React.MouseEvent) => void = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleMenuClose = () => setAnchorEl(null);

  const handleLogout = () => auth.signout();
  return (
    <div>
      <IconButton
        aria-controls="user-menu"
        aria-haspopup="true"
        onClick={handleMenuClick}
        color="inherit"
      >
        <MoreVert />
      </IconButton>
      <Menu
        id="user-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={handleLogout}>Logout</MenuItem>
      </Menu>
    </div>
  );
};

export default UserMenu;
