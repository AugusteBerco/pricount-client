import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { Transfer } from "typings";

interface IProps {
  transfer: Transfer;
}

const useStyles = makeStyles((theme) => ({
  participantName: {
    fontWeight: "bold",
    color: theme.palette.primary.main,
  },
}));
const TransferListItem: React.FC<IProps> = ({ transfer }) => {
  const classes = useStyles();
  return (
    <Card>
      <CardContent>
        <span className={classes.participantName}>{transfer.name}</span> owes{" "}
        {transfer.amount.format()} to{" "}
        <span className={classes.participantName}>{transfer.to}</span>
      </CardContent>
    </Card>
  );
};

export default TransferListItem;
