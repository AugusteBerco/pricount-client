import List from "@material-ui/core/List";
import React from "react";
import { Balance, Transfer } from "typings";
import BalanceListItem from "ui/components/BalanceListItem";
import TransferListItem from "ui/components/TransferListItem";
import { Typography } from "@material-ui/core";

interface IProps {
  balances: Balance[];
  transfers: Transfer[];
}
const BalancesTabContent: React.FC<IProps> = ({ balances, transfers }) => {
  console.log(balances);
  const balancesSum = balances.reduce((sum: Balance[], b: Balance) => {
    if (b.total.value !== 0) sum.push(b);
    return sum;
  }, []);

  if (!balancesSum.length) {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          paddingTop: 16,
        }}
      >
        <Typography variant="h4">The pricount is balanced</Typography>
      </div>
    );
  }

  const maxDebtAmount = Math.abs(balances[0].total.value);
  const maxOwnsAmount = Math.abs(balances[balances.length - 1].total.value);

  return (
    <div style={{ display: "flex", flexDirection: "column", flex: 1 }}>
      <div style={{ width: "100%" }}>
        <List>
          {balances.map((balance: Balance) => (
            <BalanceListItem
              key={balance.participant.id}
              balance={balance}
              max={
                maxDebtAmount > maxOwnsAmount ? maxDebtAmount : maxOwnsAmount
              }
              // maxOwnsAmount={maxOwnsAmount}
            />
          ))}
        </List>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          padding: "4px 4px",
        }}
      >
        {transfers.map((transfer: Transfer, index: number) => (
          // eslint-disable-next-line react/no-array-index-key
          <TransferListItem key={index} transfer={transfer} />
        ))}
      </div>
    </div>
  );
};

export default BalancesTabContent;
