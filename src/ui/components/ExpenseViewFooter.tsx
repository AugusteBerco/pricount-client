import { Button } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import React from "react";
import { Link, useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  footer: {
    // marginTop: "auto",
    width: "100%",
    padding: theme.spacing(3, 2),
    position: "fixed",
    bottom: 0,
    display: "flex",
    flexDirection: "row",
    backgroundColor: theme.palette.background.paper,
  },
  footerItem: {
    flexGrow: 1,
  },
}));

interface IProps {
  previous?: string;
  next?: string;
}

const ExpenseViewFooter: React.FC<IProps> = ({ previous, next }) => {
  const classes = useStyles();
  const history = useHistory();

  const handleNextClick = () => (next ? history.push(next) : null);

  return (
    <Box className={classes.footer}>
      <Box className={classes.footerItem}>
        {previous ? (
          <Button
            component={Link}
            to={previous}
            startIcon={<ArrowBackIosIcon />}
          >
            Previous
          </Button>
        ) : null}
      </Box>
      <Box className={classes.footerItem} style={{ textAlign: "right" }}>
        {next ? (
          <Button endIcon={<ArrowForwardIosIcon />} onClick={handleNextClick}>
            Next
          </Button>
        ) : null}
      </Box>
    </Box>
  );
};

export default ExpenseViewFooter;
