import Box from "@material-ui/core/Box";
import Fab from "@material-ui/core/Fab";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import React from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import {
  ExpenseTypes,
  GetPricountByIdQuery,
  useGetCurrentUserQuery,
} from "generated-graphql";
import currency from "currency.js";

const useStyles = makeStyles((theme) => ({
  footer: {
    width: "100%",
    padding: theme.spacing(3, 2),
    position: "fixed",
    bottom: 0,
    display: "flex",
    flexDirection: "row",
    backgroundColor: theme.palette.background.paper,
  },
  footerItem: {
    flexGrow: 1,
  },
}));

interface IProps {
  expenseList: GetPricountByIdQuery["getPricount"];
}

const ExpensesListFooter: React.FC<IProps> = ({ expenseList }) => {
  const classes = useStyles();
  const history = useHistory();
  const match = useRouteMatch();
  const { data, loading } = useGetCurrentUserQuery();

  const totalPricount = expenseList
    .expenses!.filter((expense) => expense.type === ExpenseTypes.Expense)
    .reduce(
      (total, expense) => total.add(expense.amount),
      currency(0, { symbol: "€" })
    );

  const expenses = expenseList.expenses || [];

  const totalFor = (userId?: number) => {
    const defaultTotal = currency(0, { symbol: "€" });
    if (!userId) {
      return defaultTotal;
    }
    const expensesToCount = expenses.filter(
      (expense) => expense.type === ExpenseTypes.Expense
    );
    const participant = expenseList.participants.find(
      (p) => p.user?.id === userId
    );
    const participantId = participant!.id;
    const total = expensesToCount.reduce(
      (t, expense) =>
        expense.forWhom.map((fw) => fw.id).includes(participantId)
          ? t.add(currency(expense.amount).divide(expense.forWhom.length))
          : t,
      defaultTotal
    );
    return total;
  };
  return (
    <Box className={classes.footer}>
      <div style={{ position: "absolute", top: "-20px", left: "50%" }}>
        <Fab
          size="small"
          color="secondary"
          onClick={() => history.push(`${match.url}/add-expense`)}
        >
          <AddIcon />
        </Fab>
      </div>

      <Box className={classes.footerItem}>
        MY TOTAL <br />
        {loading ? "calculating..." : totalFor(data?.currentUser.id).format()}€
      </Box>
      <Box className={classes.footerItem} style={{ textAlign: "right" }}>
        TOTAL EXPENSES <br />
        {totalPricount.format()}
      </Box>
    </Box>
  );
};

export default ExpensesListFooter;
