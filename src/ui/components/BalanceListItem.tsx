import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
import LinearProgress from "@material-ui/core/LinearProgress";
import ListItem from "@material-ui/core/ListItem";
import {
  createMuiTheme,
  createStyles,
  makeStyles,
  ThemeProvider,
  withStyles,
} from "@material-ui/core/styles";
import clsx from "clsx";
import React from "react";
import { Balance } from "typings";

interface IProps {
  balance: Balance;
  max: number;
}

const overridenTheme = (isNegative: boolean) =>
  createMuiTheme({
    direction: isNegative ? "rtl" : "ltr",
    palette: {
      primary: isNegative ? red : green,
    },
  });

const RTLLinearProgress = (props: any) => {
  const { isNegative, ...linearProps } = props;
  return (
    <ThemeProvider theme={overridenTheme(isNegative)}>
      <LinearProgress {...linearProps} />{" "}
    </ThemeProvider>
  );
};

const BalanceProgress = withStyles((theme) =>
  createStyles({
    root: {
      height: 50,
    },
    barColorSecondary: {
      backgroundColor: theme.palette.background.default,
    },
  })
)(RTLLinearProgress);

const useBalanceStyles = makeStyles((theme) => ({
  balanceContainer: {
    display: "inline-block",
    flexGrow: 1,
    textAlign: "right",
    padding: `${theme.spacing()}px ${theme.spacing(2)}px`,
    position: "relative",
  },
  textAmount: {
    position: "absolute",
    zIndex: 10,
    top: "35%",
  },
  negativeAmount: {
    right: `${theme.spacing(3)}px`,
  },
  positiveAmount: {
    left: `${theme.spacing(3)}px`,
  },
}));
const BalanceAmount: React.FC<{
  balance: Balance;
  progressValue: number;
  isNegative: boolean;
}> = ({ balance, progressValue, isNegative }) => {
  const classes = useBalanceStyles();
  return (
    <div className={classes.balanceContainer}>
      <span
        className={clsx(
          classes.textAmount,
          isNegative ? classes.negativeAmount : classes.positiveAmount
        )}
      >
        {balance.total.format()}
      </span>

      <BalanceProgress
        value={progressValue}
        variant="determinate"
        style={{ backgroundColor: "#303030" }}
        isNegative={isNegative}
      />
    </div>
  );
};

interface StylesProps {
  isNegative: boolean;
}
const useBalanceListItemStyle = makeStyles({
  balanceName: (props: StylesProps) => ({
    flex: 1,
    textAlign: props.isNegative ? "left" : "right",
  }),
  negativeBalanceName: {},
});
const BalanceListItem: React.FC<IProps> = ({ balance, max }) => {
  const isNegative = balance.total.value < 0;
  const classes = useBalanceListItemStyle({ isNegative });

  const progressValue = (Math.abs(balance.total.value) * 100) / max;

  return (
    <ListItem alignItems="center">
      {isNegative ? null : (
        <div className={classes.balanceName}>{balance.participant.name}</div>
      )}
      <BalanceAmount
        balance={balance}
        progressValue={progressValue}
        isNegative={isNegative}
      />
      {isNegative ? (
        <div className={classes.balanceName}>{balance.participant.name}</div>
      ) : null}
    </ListItem>
  );
};

export default BalanceListItem;
