import { makeStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import {
  CreatePricountInput,
  GetPricountByIdQuery,
  UpdatePricountInput,
} from "generated-graphql";
import { useCurrentUser, UserProvider } from "hooks/useCurrentUser";
import React, { Fragment } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import { toast } from "react-toastify";
import FormLayout from "ui/layouts/FormLayout";
import ParticipantInput from "./ParticipantInput";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    "& > *": {
      margin: theme.spacing(1),
      // width: "25ch",
    },
  },
}));

interface IProps<T> {
  list?: GetPricountByIdQuery["getPricount"];
  saveList(d: T): Promise<void>;
}

const ExpenseListForm: React.FC<
  IProps<CreatePricountInput | UpdatePricountInput>
> = ({ list, saveList }) => {
  const classes = useStyles();
  const { user } = useCurrentUser() as UserProvider;

  const { register, handleSubmit, errors, control } = useForm({
    defaultValues: {
      title: list ? list.title : "",
      description: list ? list.description! : "",
      participants: list
        ? list.participants.map((p) => ({
            id: p.id,
            name: p.name,
            user: p.user?.id,
          }))
        : [
            { name: user.username, id: null },
            { name: "", id: "" },
          ],
    },
  });
  const { fields, remove, insert } = useFieldArray({
    control,
    name: "participants",
    keyName: "_id",
  });

  const handleAddPeople = (index: number) => {
    insert(index + 1, { name: "", id: "" });
  };

  const handleRemovePeople = async (index: number) => {
    if (fields[index].user === user.id) {
      toast.error("You cannot remove yourself from a pricount");
      return;
    }
    if (list && fields[index].id) {
      const participantExpense = list.expenses?.find((expense) => {
        return (
          expense.paidBy.id === fields[index].id ||
          expense.forWhom.find((fw) => fw.id === fields[index].id)
        );
      });

      if (participantExpense) {
        toast.error(
          "This participant is part of expenses and cannot be removed"
        );
        return;
      }
    }
    remove(index);
  };

  return (
    <FormLayout
      title={list ? "Edit pricount" : "New List"}
      handleCheck={handleSubmit(saveList)}
    >
      <form className={classes.form} noValidate>
        <TextField
          name="title"
          inputRef={register}
          // variant="outlined"
          label="Name of the list"
          fullWidth
          error={!!errors.title}
          helperText={errors.title?.message}
        />
        <TextField
          name="description"
          inputRef={register}
          // variant="outlined"
          label="Description"
          fullWidth
          error={!!errors.title}
          helperText={errors.title?.message}
          inputProps={{
            maxLength: 500,
          }}
        />

        <Typography variant="h5">Participants</Typography>
        <Grid container spacing={1}>
          {fields.map((person, index) => (
            <Fragment key={`person-${person._id}`}>
              <ParticipantInput
                add={handleAddPeople}
                remove={handleRemovePeople}
                participant={person}
                index={index}
                register={register}
                canRemove={fields.length > 1}
                user={user}
              />
            </Fragment>
          ))}
        </Grid>
      </form>
    </FormLayout>
  );
};

export default ExpenseListForm;
