import { Button, useTheme } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { AutoRotatingCarousel, Slide } from "material-auto-rotating-carousel";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useAuth, AuthProvider } from "hooks/user-auth";
// import useAccount from "/imports/hooks/useAccount";
import Layout from "../layouts/Layout";

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(4),
  },
}));
const HomePage = () => {
  const theme = useTheme();
  const classes = useStyles();
  const history = useHistory();
  const auth = useAuth() as AuthProvider;

  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (auth.isLoggedIn) {
      // history.push("/expenses");
    }
  }, [auth.isLoggedIn, history]);

  const handleOnStart = () => {
    setOpen(false);
    history.push("/register");
  };

  return (
    <Layout>
      <Container className={classes.container}>
        <Typography variant="h4">Welcome to Pricount</Typography>
        <Button onClick={() => setOpen(true)}>Start Using</Button>
        <AutoRotatingCarousel
          label="Get started"
          open={open}
          onClose={() => setOpen(false)}
          onStart={handleOnStart}
          mobile
          autoplay={false}
          style={{ position: "absolute" }}
        >
          {/* <Slide
            // media={<img src='http://www.icons101.com/icon_png/size_256/id_79394/youtube.png' />}
            mediaBackgroundStyle={{ backgroundColor: red[400] }}
            style={{ backgroundColor: red[600] }}
            title="Welcome to Pricount"
            subtitle="A privacy oriented app"
          /> */}
          <Slide
            media={
              <img
                src="/undraw_investing_7u74.svg"
                width="300"
                alt="investing"
              />
            }
            mediaBackgroundStyle={{
              backgroundColor: theme.palette.primary.main,
            }}
            style={{ backgroundColor: theme.palette.primary.dark }}
            title="Track your expenses"
            subtitle="Count how much was spent by whom"
          />
          <Slide
            media={
              <img
                src="/undraw_transfer_money_rywa.svg"
                width="300"
                alt="transfer money"
              />
            }
            mediaBackgroundStyle={{
              backgroundColor: theme.palette.primary.main,
            }}
            style={{ backgroundColor: theme.palette.primary.dark }}
            title="Settle debts"
            subtitle="Keep track of debts, transfers and expenses"
          />
          <Slide
            media={
              <img
                src="/undraw_privacy_protection_nlwy.svg"
                width="300"
                alt="privacy protection"
              />
            }
            mediaBackgroundStyle={{
              backgroundColor: theme.palette.primary.main,
            }}
            style={{ backgroundColor: theme.palette.primary.dark }}
            title="Privately"
            subtitle="No data will be shared with anyone"
          />

          <Slide
            media={
              <img
                src="/undraw_team_work_k80m.svg"
                width="300"
                alt="team work"
              />
            }
            mediaBackgroundStyle={{
              backgroundColor: theme.palette.primary.main,
            }}
            style={{ backgroundColor: theme.palette.primary.dark }}
            title="Keep your friends"
            subtitle="Good counts make good friends"
          />
        </AutoRotatingCarousel>
      </Container>
    </Layout>
  );
};

export default HomePage;
