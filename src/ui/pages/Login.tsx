import React from "react";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Container, Grid, makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import Layout from "../layouts/Layout";
import { useAuth, AuthProvider } from "hooks/user-auth";

interface LoginFormInputs {
  username: string;
  password: string;
}
const LoginFormSchema = yup.object().shape({
  username: yup.string().required(),
  password: yup.string().required(),
});

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LoginPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const auth = useAuth() as AuthProvider;
  const handleLogin = async (d: LoginFormInputs) => {
    try {
      await auth.signin(d.username, d.password);
    } catch (e) {
      toast.error("An error occurred");
      console.log(e);
    }

    history.push("/");
  };

  const { register, handleSubmit, errors } = useForm<LoginFormInputs>({
    resolver: yupResolver(LoginFormSchema),
  });

  console.log(errors);

  return (
    <Layout>
      <Container>
        <div className={classes.paper}>
          <Typography variant="h2">Sign In</Typography>

          <form
            className={classes.form}
            onSubmit={handleSubmit(handleLogin)}
            noValidate
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  name="username"
                  variant="outlined"
                  color="secondary"
                  inputRef={register}
                  error={!!errors.username}
                  helperText={errors.username?.message}
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  autoComplete="email"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type="password"
                  name="password"
                  variant="outlined"
                  color="secondary"
                  inputRef={register}
                  error={!!errors.password}
                  helperText={errors.password?.message}
                  id="password"
                  label="Password"
                  autoComplete="current-password"
                  required
                  fullWidth
                />
              </Grid>
            </Grid>

            <Button
              type="submit"
              className={classes.submit}
              fullWidth
              variant="contained"
              color="primary"
            >
              Login
            </Button>
          </form>
        </div>
      </Container>
    </Layout>
  );
};

export default LoginPage;
