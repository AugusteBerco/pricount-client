import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { makeStyles } from "@material-ui/core";
import { Link, useHistory } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { toast } from "react-toastify";
import Layout from "../layouts/Layout";
import { RegisterFormInputs } from "typings";
import { AuthProvider, useAuth } from "hooks/user-auth";

const RegistrationFormSchema = yup.object().shape({
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().required(),
  confirmPassword: yup
    .string()
    // .required()
    .oneOf([yup.ref("password"), null], "Passwords don't match"),
});

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const RegisterPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const auth = useAuth() as AuthProvider;
  const { register, handleSubmit, errors } = useForm<RegisterFormInputs>({
    resolver: yupResolver(RegistrationFormSchema),
  });

  console.log(errors);
  const handleRegistration = async (d: RegisterFormInputs) => {
    try {
      await auth.signup(d);
    } catch (e) {
      toast.error("An error occurred");
      console.log(e);
    }

    history.push("/");
  };
  return (
    <Layout>
      <Container>
        <div className={classes.paper}>
          <Typography variant="h2">Sign In</Typography>

          <form
            className={classes.form}
            onSubmit={handleSubmit(handleRegistration)}
            noValidate
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  color="secondary"
                  name="username"
                  inputRef={register}
                  error={!!errors.username}
                  helperText={errors.username?.message}
                  required
                  fullWidth
                  id="username"
                  label="User Name"
                  autoComplete="username"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  color="secondary"
                  name="email"
                  inputRef={register}
                  error={!!errors.email}
                  helperText={errors.email?.message}
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type="password"
                  color="secondary"
                  name="password"
                  inputRef={register}
                  error={!!errors.password}
                  helperText={errors.password?.message}
                  id="password"
                  label="Password"
                  autoComplete="current-password"
                  required
                  fullWidth
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  color="secondary"
                  type="password"
                  name="repeat_password"
                  inputRef={register}
                  error={!!errors.repeat_password}
                  helperText={errors.repeat_password?.message}
                  id="password"
                  label="Confirm Password"
                  required
                  fullWidth
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              className={classes.submit}
              fullWidth
              variant="contained"
              color="secondary"
            >
              Register
            </Button>
            <Typography variant="body1">
              Already have an account? <Link to="/login">Login</Link>
            </Typography>
          </form>
        </div>
      </Container>
    </Layout>
  );
};

export default RegisterPage;
