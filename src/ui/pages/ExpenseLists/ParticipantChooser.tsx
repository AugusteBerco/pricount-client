import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormLabel from "@material-ui/core/FormLabel";
import {
  GetPricountByIdQuery,
  useAssignUserToParticipantMutation,
} from "generated-graphql";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-toastify";
import FormLayout from "ui/layouts/FormLayout";

interface IProps {
  listId: number;
  participants: GetPricountByIdQuery["getPricount"]["participants"];
}

const ParticipantChooser: React.FC<IProps> = ({ listId, participants }) => {
  const { handleSubmit, control } = useForm();

  const [assignUser] = useAssignUserToParticipantMutation({
    onError(err) {
      toast.error(err.message);
    },
  });

  const submitParticipant = async (d: any) => {
    const data = await assignUser({
      variables: {
        id: listId,
        participantId: parseInt(Object.keys(d)[0]),
      },
    });

    console.log(data);
  };

  return (
    <FormLayout title="Choose" handleCheck={handleSubmit(submitParticipant)}>
      <FormLabel component="legend">Who are you?</FormLabel>
      <FormGroup>
        {participants.map((participant) => (
          <FormControlLabel
            key={participant.id}
            control={
              <Controller
                name={`${participant.id}`}
                control={control}
                defaultValue={false}
                render={(props) => (
                  <Checkbox
                    checked={props.value}
                    onChange={(e) => props.onChange(e.currentTarget.checked)}
                  />
                )}
              />
            }
            label={participant.name}
          />
        ))}
      </FormGroup>
    </FormLayout>
  );
};

export default ParticipantChooser;
