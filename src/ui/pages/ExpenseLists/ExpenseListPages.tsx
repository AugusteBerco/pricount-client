import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { useGetPricountByIdQuery } from "generated-graphql";
import { useCurrentUser, UserProvider } from "hooks/useCurrentUser";
import { Route, Switch, useParams, useRouteMatch } from "react-router-dom";
import Loading from "ui/components/Loading";
import MasterLayout from "ui/layouts/MasterLayout";
import ExpenseListEdit from "ui/pages/ExpenseLists/ExpenseListEdit";
import ExpenseListPage from "ui/pages/ExpenseLists/ExpenseListPage";
import ExpensePage from "ui/pages/ExpenseLists/ExpensePages";
import NewExpenseForm from "ui/pages/ExpenseLists/NewExpenseForm";
import ParticipantChooser from "ui/pages/ExpenseLists/ParticipantChooser";

const ExpenseListPages = () => {
  const { expenseListId } = useParams<{ expenseListId: string }>();
  const { user } = useCurrentUser() as UserProvider;

  const match = useRouteMatch();

  const { data, loading } = useGetPricountByIdQuery({
    variables: {
      id: parseInt(expenseListId),
    },
  });

  const expenseList = data?.getPricount;
  const expenses = data?.getPricount.expenses;

  if (loading || !expenseList) {
    return (
      <MasterLayout
        appBarContent={
          <Toolbar>
            <Typography variant="h6">MoneyTrack</Typography>
          </Toolbar>
        }
      >
        <Loading />
      </MasterLayout>
    );
  }

  const userIsParticipant = expenseList.participants.find(
    (p) => p.user?.id === user?.id
  );

  if (!userIsParticipant) {
    const assignedParticipants = expenseList.participants
      .map((p) => p.user?.id)
      .filter((id) => id);
    if (assignedParticipants.length === expenseList.participants.length) {
      return <p>error</p>;
    }

    return (
      <ParticipantChooser
        listId={expenseList.id}
        participants={expenseList.participants.filter(
          (p) => p.user?.id == null
        )}
      />
    );
  }

  return (
    <Switch>
      <Route path={`${match.path}/expense/:expenseId`}>
        <ExpensePage
          firstExpense={
            expenses && expenses.length ? expenses[0].id : undefined
          }
          lastExpense={
            expenses && expenses.length
              ? expenses[expenses.length - 1].id
              : undefined
          }
          participants={expenseList.participants}
        />
      </Route>
      <Route path={`${match.path}/add-expense`}>
        <NewExpenseForm expenseList={expenseList} />
      </Route>
      <Route path={`${match.path}/edit`}>
        <ExpenseListEdit expenseList={expenseList} />
      </Route>
      <Route path={`${match.path}`}>
        <ExpenseListPage
          expenses={expenses}
          expenseList={expenseList}
          // transfers={expenseList!.transfers()}
          // balances={expenseList!.balances()}
          participants={expenseList.participants}
        />
      </Route>
    </Switch>
  );
  // <ExpenseListLayout title={expenseList?.title} />;
};

export default ExpenseListPages;
