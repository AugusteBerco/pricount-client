import React from "react";
import { toast } from "react-toastify";
import { useHistory, useParams } from "react-router-dom";
import { ExpenseInput } from "typings";
import ExpenseForm from "ui/components/ExpenseForm";
import { pickBy } from "lodash";
import {
  ExpenseFieldsFragment,
  useUpdateExpenseMutation,
  ExpenseCategories,
  ExpenseTypes,
} from "generated-graphql";

interface IProps {
  expense: ExpenseFieldsFragment;
  participants: any[];
}

const ExpenseEditPage: React.FC<IProps> = ({ expense, participants }) => {
  const history = useHistory();
  const { expenseListId } = useParams<{
    expenseListId: string;
    exoenseId: string;
  }>();

  const [updateExpenseMutation] = useUpdateExpenseMutation({
    onError(error) {
      toast.error(error.message);
    },
  });

  const saveExpense = async (d: ExpenseInput) => {
    const { data } = await updateExpenseMutation({
      variables: {
        updateExpenseInput: {
          id: expense.id,
          title: d.title,
          type: d.expenseType as ExpenseTypes,
          category: d.category as ExpenseCategories,
          doneAt: new Date(d.date),
          amount: d.amount as number,
          paidBy: parseInt(d.paidBy),
          forWhom: Object.keys(pickBy(d.forWhom, (v, k) => v)).map((k) =>
            parseInt(k)
          ),
        },
      },
    });
    if (data) {
      history.push(`/expenses/${expenseListId}`);
    }
  };
  return (
    <ExpenseForm
      expense={expense}
      participants={participants}
      saveExpense={saveExpense}
    />
  );
};

export default ExpenseEditPage;
