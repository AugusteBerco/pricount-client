import { Box } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import currency from "currency.js";
import { format } from "date-fns";
import {
  ExpenseFieldsFragment,
  ExpenseTypes,
  ParticipantInfoFragment,
} from "generated-graphql";
import { useCurrentUser, UserProvider } from "hooks/useCurrentUser";
import React from "react";
import { generatePath, useHistory, useRouteMatch } from "react-router-dom";
import ExpenseViewFooter from "ui/components/ExpenseViewFooter";
import MasterLayout from "ui/layouts/MasterLayout";

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
    marginTop: theme.spacing(8),
  },
  subtitle: {
    flexGrow: 1,
    marginTop: theme.spacing(2),
  },
  paidBy: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(2),
  },
  forwhomBox: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[700],
  },
}));

interface IProps {
  expense: ExpenseFieldsFragment;
  participants: ParticipantInfoFragment[];
}

const ExpenseViewPage: React.FC<IProps> = ({ expense, participants }) => {
  const history = useHistory();
  const match = useRouteMatch<{ expenseListId: string }>();
  const classes = useStyles();
  const { user } = useCurrentUser() as UserProvider;

  const userId = user?.id;

  const paidByParticipant = participants.find(
    (p) => p.id === expense.paidBy.id
  );

  const userIsParticipant = participants.find((p) => p.user?.id === userId);

  const handleBack = () =>
    history.push(`/expenses/${match.params.expenseListId}`);

  const previousId = expense.previous?.id;
  const nextId = expense.next?.id;

  const generateExpensePath = (expenseId?: number) =>
    expenseId
      ? generatePath(match.path, {
          expenseId: expenseId,
          expenseListId: match.params.expenseListId,
        })
      : undefined;

  const previous = generateExpensePath(previousId);

  const next = generateExpensePath(nextId);

  return (
    <MasterLayout
      appBarContent={
        <>
          <Toolbar style={{ alignItems: "start" }}>
            <IconButton onClick={handleBack} color="inherit">
              <ArrowBackIcon />
            </IconButton>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                flexGrow: 1,
                alignItems: "center",
              }}
            >
              <Typography variant="h4" className={classes.title}>
                {expense.title}
              </Typography>
              {expense.type === ExpenseTypes.Transfer ? (
                <Typography variant="body1">Money Transfer</Typography>
              ) : null}
              <Typography variant="h5" className={classes.subtitle}>
                €{expense.amount}
              </Typography>
            </div>
            <Button
              variant="text"
              color="inherit"
              onClick={() => history.push(`${match.url}/edit`)}
            >
              EDIT
            </Button>
          </Toolbar>
          <Box className={classes.paidBy}>
            <Typography variant="h6">
              paid by{" "}
              {paidByParticipant
                ? paidByParticipant.name
                : "Participant not found"}
            </Typography>
            <Typography variant="h6">
              {format(new Date(expense.doneAt), "dd/MM/yyyy")}
            </Typography>
          </Box>
        </>
      }
    >
      <Box className={classes.forwhomBox}>
        <Typography variant="subtitle1">
          For {expense.forWhom.length} participant{" "}
          {userIsParticipant ? ", including me" : null}
        </Typography>
      </Box>
      <List component="nav" aria-label="expense for">
        {expense.forWhom.map((p) => (
          <ListItem key={p.id}>
            <ListItemText primary={p.name} />
            <ListItemSecondaryAction>
              {currency(expense.amount, { symbol: "€" })
                .divide(expense.forWhom.length)
                .format()}
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>

      <ExpenseViewFooter previous={previous} next={next} />
    </MasterLayout>
  );
};

export default ExpenseViewPage;
