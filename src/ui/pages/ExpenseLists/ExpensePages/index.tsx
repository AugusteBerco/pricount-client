import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {
  ParticipantInfoFragment,
  useGetExpenseByIdQuery,
} from "generated-graphql";
import React from "react";
import { Route, Switch, useParams, useRouteMatch } from "react-router-dom";
import Loading from "ui/components/Loading";
import MasterLayout from "ui/layouts/MasterLayout";
import ExpenseEditPage from "./ExpenseEdit";
import ExpenseViewPage from "./ExpenseView";

interface IProps {
  firstExpense?: number;
  lastExpense?: number;
  participants: ParticipantInfoFragment[];
}

const ExpensePage: React.FC<IProps> = ({ participants }) => {
  const { expenseId } = useParams<{
    expenseId: string;
  }>();
  const match = useRouteMatch();

  const { data, loading } = useGetExpenseByIdQuery({
    variables: {
      expenseId: parseInt(expenseId),
    },
  });

  if (loading || !data) {
    return (
      <MasterLayout
        appBarContent={
          <Toolbar>
            <Typography variant="h6">MoneyTrack</Typography>
          </Toolbar>
        }
      >
        <Loading />
      </MasterLayout>
    );
  }

  const { expense } = data!;
  // const { participants } = expense!;

  return (
    <Switch>
      <Route path={`${match.path}/edit`}>
        <ExpenseEditPage expense={expense} participants={participants} />
      </Route>
      <Route path={`${match.path}`}>
        <ExpenseViewPage expense={expense} participants={participants} />
      </Route>
    </Switch>
  );
};

export default ExpensePage;
