import React from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import { pickBy } from "lodash";
import { ExpenseInput } from "typings";
import ExpenseForm from "ui/components/ExpenseForm";
import {
  ExpenseCategories,
  ExpenseTypes,
  GetPricountByIdQuery,
  useCreateExpenseMutation,
  ExpenseFieldsFragmentDoc,
} from "generated-graphql";

interface IProps {
  expenseList: GetPricountByIdQuery["getPricount"];
}

const NewExpenseForm: React.FC<IProps> = ({ expenseList }) => {
  const history = useHistory();
  const [createExpenseMutation] = useCreateExpenseMutation({
    update(cache, { data }) {
      cache.modify({
        id: cache.identify(expenseList),
        fields: {
          expenses(pricountExpenseList = []) {
            const newExpenseRef = cache.writeFragment({
              data: data!.createExpense,
              fragment: ExpenseFieldsFragmentDoc,
              fragmentName: "ExpenseFields",
            });
            return [...pricountExpenseList, newExpenseRef];
          },
        },
      });
    },
    onError(error) {
      toast.error(error.message);
    },
  });

  const saveExpense = async (d: ExpenseInput) => {
    console.log(d);
    const { data } = await createExpenseMutation({
      variables: {
        createExpenseInput: {
          pricountId: expenseList.id,
          title: d.title,
          type: d.expenseType as ExpenseTypes,
          category: d.category as ExpenseCategories,
          doneAt: new Date(d.date),
          amount: d.amount as number,
          paidBy: parseInt(d.paidBy),
          forWhom: Object.keys(pickBy(d.forWhom, (v, k) => v)).map((k) =>
            parseInt(k)
          ),
        },
      },
    });

    if (data && data.createExpense) {
      history.push(`/expenses/${expenseList.id}`);
    }
  };
  return (
    <ExpenseForm
      saveExpense={saveExpense}
      participants={expenseList.participants}
    />
  );
};

export default NewExpenseForm;
