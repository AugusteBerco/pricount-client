import AddIcon from "@material-ui/icons/Add";
import SpeedDial from "@material-ui/lab/SpeedDial";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import React from "react";
import { Route, Switch, useRouteMatch, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import ExpensesListPages from "ui/pages/ExpenseLists/ExpenseListPages";
import ExpenseListsList from "ui/components/ExpenseListsList";
import Layout from "ui/layouts/Layout";
import ListForm from "./ListForm";

const useStyles = makeStyles((theme) => ({
  speedDial: {
    position: "absolute",
    "&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft": {
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    "&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight": {
      top: theme.spacing(2),
      left: theme.spacing(2),
    },
  },
}));

const ExpenseListsIndex = () => {
  const classes = useStyles();
  const match = useRouteMatch();
  const history = useHistory();

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleCreateList = () => {
    history.push(`${match.url}/new`);
    handleClose();
  };
  return (
    <Switch>
      <Route path={`${match.url}/new`} strict>
        <ListForm />
      </Route>
      <Route path={`${match.url}/:expenseListId`}>
        <ExpensesListPages />
      </Route>

      <Route path={`${match.url}`} strict>
        <Layout>
          <ExpenseListsList />
          <SpeedDial
            ariaLabel="SpeedDial example"
            className={classes.speedDial}
            icon={<AddIcon />}
            onClose={handleClose}
            onOpen={handleOpen}
            open={open}
            direction="up"
          >
            <SpeedDialAction
              icon={<AddIcon />}
              tooltipTitle="Create new expense list"
              onClick={handleCreateList}
            />
          </SpeedDial>
        </Layout>
      </Route>
    </Switch>
  );
};

export default ExpenseListsIndex;
