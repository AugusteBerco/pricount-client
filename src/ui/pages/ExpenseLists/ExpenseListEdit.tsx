import {
  GetPricountByIdQuery,
  UpdatePricountInput,
  useUpdatePricountMutation,
} from "generated-graphql";
import React from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import ExpenseListForm from "ui/components/ExpenseListForm";

interface IProps {
  expenseList: GetPricountByIdQuery["getPricount"];
}
const ExpenseListEdit: React.FC<IProps> = ({ expenseList }) => {
  const history = useHistory();

  const [updatePricountMutation] = useUpdatePricountMutation({
    onError(error) {
      toast.error(error.message);
    },
  });

  async function saveList(d: UpdatePricountInput) {
    console.log("saving");
    console.log(d);
    const { data } = await updatePricountMutation({
      variables: {
        updatePricountInput: {
          id: expenseList.id,
          title: d.title,
          // ownerName: d.ownerName,
          description: d.description,
          participants: d.participants.map((o) => ({
            id: parseInt(o.id + ""),
            name: o.name,
          })),
        },
      },
    });
    if (data) {
      history.push(`/expenses/${expenseList.id}`);
    }
    console.log(data);
  }
  return <ExpenseListForm list={expenseList} saveList={saveList} />;
};

export default ExpenseListEdit;
