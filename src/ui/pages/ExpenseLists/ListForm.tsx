import {
  CreatePricountInput,
  Pricount,
  useCreatePricountMutation,
} from "generated-graphql";
import React from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import ExpenseListForm from "ui/components/ExpenseListForm";

interface IProps {
  list?: Pricount;
}

const ListForm: React.FC<IProps> = ({ list }) => {
  const history = useHistory();
  const [createPricount] = useCreatePricountMutation({
    onCompleted: ({ createPricount }) => {
      history.push(`/expenses/${createPricount.id}`);
    },
    onError: (err) => {
      toast.error(err.message);
    },
  });

  const saveList = async (d: CreatePricountInput) => {
    await createPricount({
      variables: {
        pricountInput: {
          title: d.title,
          // ownerName: d.ownerName,
          description: d.description,
          participants: d.participants.map((p) => ({ name: p.name })),
        },
      },
    });
  };

  return <ExpenseListForm list={list} saveList={saveList} />;
};

export default ListForm;
