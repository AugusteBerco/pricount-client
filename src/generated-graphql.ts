import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
};

export type CreateExpenseInput = {
  title: Scalars['String'];
  amount: Scalars['Float'];
  category: ExpenseCategories;
  type: ExpenseTypes;
  doneAt: Scalars['DateTime'];
  paidBy: Scalars['Int'];
  forWhom: Array<Scalars['Int']>;
  pricountId: Scalars['Int'];
};

export type CreatePricountInput = {
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  participants: Array<ParticipantInput>;
};


export type Expense = {
  __typename?: 'Expense';
  id: Scalars['Int'];
  title: Scalars['String'];
  amount: Scalars['Float'];
  category: ExpenseCategories;
  type: ExpenseTypes;
  doneAt: Scalars['DateTime'];
  paidBy: Participant;
  forWhom: Array<Participant>;
  previous?: Maybe<Expense>;
  next?: Maybe<Expense>;
};

export enum ExpenseCategories {
  Unset = 'UNSET',
  Rent = 'RENT',
  Accomodation = 'ACCOMODATION',
  Fun = 'FUN',
  Groceries = 'GROCERIES',
  Health = 'HEALTH',
  Insurance = 'INSURANCE',
  Restaurants = 'RESTAURANTS',
  Shopping = 'SHOPPING',
  Transport = 'TRANSPORT'
}

export enum ExpenseTypes {
  Expense = 'EXPENSE',
  Transfer = 'TRANSFER'
}

export type Mutation = {
  __typename?: 'Mutation';
  createPricount: Pricount;
  updatePricount: Pricount;
  assignUserToParticipant: Pricount;
  createExpense: Expense;
  updateExpense: Expense;
  removeExpense: Expense;
};


export type MutationCreatePricountArgs = {
  createPricountInput: CreatePricountInput;
};


export type MutationUpdatePricountArgs = {
  updatePricountInput: UpdatePricountInput;
};


export type MutationAssignUserToParticipantArgs = {
  participantId: Scalars['Int'];
  id: Scalars['Int'];
};


export type MutationCreateExpenseArgs = {
  createExpenseInput: CreateExpenseInput;
};


export type MutationUpdateExpenseArgs = {
  updateExpenseInput: UpdateExpenseInput;
};


export type MutationRemoveExpenseArgs = {
  id: Scalars['Int'];
};

export type Participant = {
  __typename?: 'Participant';
  id: Scalars['Int'];
  name: Scalars['String'];
  user?: Maybe<User>;
};

export type ParticipantInput = {
  id?: Maybe<Scalars['Int']>;
  _id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
};

export type Pricount = {
  __typename?: 'Pricount';
  id: Scalars['Int'];
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  participants: Array<Participant>;
  expenses: Array<Expense>;
};

export type Query = {
  __typename?: 'Query';
  getUser: User;
  currentUser: User;
  getParticipant: Participant;
  participant: Participant;
  getPricountsForUser: Array<Pricount>;
  getPricount: Pricount;
  expense: Expense;
};


export type QueryGetUserArgs = {
  id: Scalars['Int'];
};


export type QueryGetParticipantArgs = {
  id: Scalars['Int'];
};


export type QueryParticipantArgs = {
  id: Scalars['Int'];
};


export type QueryGetPricountArgs = {
  id: Scalars['Int'];
};


export type QueryExpenseArgs = {
  id: Scalars['Int'];
};

export type UpdateExpenseInput = {
  title: Scalars['String'];
  amount: Scalars['Float'];
  category: ExpenseCategories;
  type: ExpenseTypes;
  doneAt: Scalars['DateTime'];
  paidBy: Scalars['Int'];
  forWhom: Array<Scalars['Int']>;
  pricountId?: Maybe<Scalars['Int']>;
  id: Scalars['Int'];
};

export type UpdatePricountInput = {
  id: Scalars['Int'];
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  participants: Array<ParticipantInput>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['Int'];
  username: Scalars['String'];
  email: Scalars['String'];
};

export type GetUserPricountsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetUserPricountsQuery = (
  { __typename?: 'Query' }
  & { getPricountsForUser: Array<(
    { __typename?: 'Pricount' }
    & Pick<Pricount, 'id' | 'title' | 'description'>
  )> }
);

export type ExpenseFieldsFragment = (
  { __typename?: 'Expense' }
  & Pick<Expense, 'id' | 'title' | 'amount' | 'type' | 'category' | 'doneAt'>
  & { paidBy: (
    { __typename?: 'Participant' }
    & ParticipantInfoFragment
  ), forWhom: Array<(
    { __typename?: 'Participant' }
    & ParticipantInfoFragment
  )>, next?: Maybe<(
    { __typename?: 'Expense' }
    & Pick<Expense, 'id'>
  )>, previous?: Maybe<(
    { __typename?: 'Expense' }
    & Pick<Expense, 'id'>
  )> }
);

export type ParticipantInfoFragment = (
  { __typename?: 'Participant' }
  & Pick<Participant, 'id' | 'name'>
  & { user?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id' | 'username'>
  )> }
);

export type PricountFieldsFragment = (
  { __typename?: 'Pricount' }
  & Pick<Pricount, 'id' | 'title' | 'description'>
  & { participants: Array<(
    { __typename?: 'Participant' }
    & ParticipantInfoFragment
  )>, expenses: Array<(
    { __typename?: 'Expense' }
    & ExpenseFieldsFragment
  )> }
);

export type GetCurrentUserQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCurrentUserQuery = (
  { __typename?: 'Query' }
  & { currentUser: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'username'>
  ) }
);

export type GetPricountByIdQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetPricountByIdQuery = (
  { __typename?: 'Query' }
  & { getPricount: (
    { __typename?: 'Pricount' }
    & Pick<Pricount, 'id' | 'title' | 'description'>
    & { participants: Array<(
      { __typename?: 'Participant' }
      & ParticipantInfoFragment
    )>, expenses: Array<(
      { __typename?: 'Expense' }
      & ExpenseFieldsFragment
    )> }
  ) }
);

export type GetExpenseByIdQueryVariables = Exact<{
  expenseId: Scalars['Int'];
}>;


export type GetExpenseByIdQuery = (
  { __typename?: 'Query' }
  & { expense: (
    { __typename?: 'Expense' }
    & ExpenseFieldsFragment
  ) }
);

export type CreatePricountMutationVariables = Exact<{
  pricountInput: CreatePricountInput;
}>;


export type CreatePricountMutation = (
  { __typename?: 'Mutation' }
  & { createPricount: (
    { __typename?: 'Pricount' }
    & Pick<Pricount, 'id' | 'title' | 'description'>
  ) }
);

export type UpdatePricountMutationVariables = Exact<{
  updatePricountInput: UpdatePricountInput;
}>;


export type UpdatePricountMutation = (
  { __typename?: 'Mutation' }
  & { updatePricount: (
    { __typename?: 'Pricount' }
    & PricountFieldsFragment
  ) }
);

export type CreateExpenseMutationVariables = Exact<{
  createExpenseInput: CreateExpenseInput;
}>;


export type CreateExpenseMutation = (
  { __typename?: 'Mutation' }
  & { createExpense: (
    { __typename?: 'Expense' }
    & ExpenseFieldsFragment
  ) }
);

export type UpdateExpenseMutationVariables = Exact<{
  updateExpenseInput: UpdateExpenseInput;
}>;


export type UpdateExpenseMutation = (
  { __typename?: 'Mutation' }
  & { updateExpense: (
    { __typename?: 'Expense' }
    & ExpenseFieldsFragment
  ) }
);

export type AssignUserToParticipantMutationVariables = Exact<{
  id: Scalars['Int'];
  participantId: Scalars['Int'];
}>;


export type AssignUserToParticipantMutation = (
  { __typename?: 'Mutation' }
  & { assignUserToParticipant: (
    { __typename?: 'Pricount' }
    & Pick<Pricount, 'id'>
    & { participants: Array<(
      { __typename?: 'Participant' }
      & ParticipantInfoFragment
    )> }
  ) }
);

export const ParticipantInfoFragmentDoc = gql`
    fragment ParticipantInfo on Participant {
  id
  user {
    id
    username
  }
  name
}
    `;
export const ExpenseFieldsFragmentDoc = gql`
    fragment ExpenseFields on Expense {
  id
  title
  amount
  type
  category
  doneAt
  paidBy {
    ...ParticipantInfo
  }
  forWhom {
    ...ParticipantInfo
  }
  next {
    id
  }
  previous {
    id
  }
}
    ${ParticipantInfoFragmentDoc}`;
export const PricountFieldsFragmentDoc = gql`
    fragment PricountFields on Pricount {
  id
  title
  description
  participants {
    ...ParticipantInfo
  }
  expenses {
    ...ExpenseFields
  }
}
    ${ParticipantInfoFragmentDoc}
${ExpenseFieldsFragmentDoc}`;
export const GetUserPricountsDocument = gql`
    query getUserPricounts {
  getPricountsForUser {
    id
    title
    description
  }
}
    `;

/**
 * __useGetUserPricountsQuery__
 *
 * To run a query within a React component, call `useGetUserPricountsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserPricountsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserPricountsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUserPricountsQuery(baseOptions?: Apollo.QueryHookOptions<GetUserPricountsQuery, GetUserPricountsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUserPricountsQuery, GetUserPricountsQueryVariables>(GetUserPricountsDocument, options);
      }
export function useGetUserPricountsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUserPricountsQuery, GetUserPricountsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUserPricountsQuery, GetUserPricountsQueryVariables>(GetUserPricountsDocument, options);
        }
export type GetUserPricountsQueryHookResult = ReturnType<typeof useGetUserPricountsQuery>;
export type GetUserPricountsLazyQueryHookResult = ReturnType<typeof useGetUserPricountsLazyQuery>;
export type GetUserPricountsQueryResult = Apollo.QueryResult<GetUserPricountsQuery, GetUserPricountsQueryVariables>;
export const GetCurrentUserDocument = gql`
    query getCurrentUser {
  currentUser {
    id
    username
  }
}
    `;

/**
 * __useGetCurrentUserQuery__
 *
 * To run a query within a React component, call `useGetCurrentUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCurrentUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCurrentUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCurrentUserQuery(baseOptions?: Apollo.QueryHookOptions<GetCurrentUserQuery, GetCurrentUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCurrentUserQuery, GetCurrentUserQueryVariables>(GetCurrentUserDocument, options);
      }
export function useGetCurrentUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCurrentUserQuery, GetCurrentUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCurrentUserQuery, GetCurrentUserQueryVariables>(GetCurrentUserDocument, options);
        }
export type GetCurrentUserQueryHookResult = ReturnType<typeof useGetCurrentUserQuery>;
export type GetCurrentUserLazyQueryHookResult = ReturnType<typeof useGetCurrentUserLazyQuery>;
export type GetCurrentUserQueryResult = Apollo.QueryResult<GetCurrentUserQuery, GetCurrentUserQueryVariables>;
export const GetPricountByIdDocument = gql`
    query getPricountById($id: Int!) {
  getPricount(id: $id) {
    id
    title
    description
    participants {
      ...ParticipantInfo
    }
    expenses {
      ...ExpenseFields
    }
  }
}
    ${ParticipantInfoFragmentDoc}
${ExpenseFieldsFragmentDoc}`;

/**
 * __useGetPricountByIdQuery__
 *
 * To run a query within a React component, call `useGetPricountByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPricountByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPricountByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetPricountByIdQuery(baseOptions: Apollo.QueryHookOptions<GetPricountByIdQuery, GetPricountByIdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPricountByIdQuery, GetPricountByIdQueryVariables>(GetPricountByIdDocument, options);
      }
export function useGetPricountByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPricountByIdQuery, GetPricountByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPricountByIdQuery, GetPricountByIdQueryVariables>(GetPricountByIdDocument, options);
        }
export type GetPricountByIdQueryHookResult = ReturnType<typeof useGetPricountByIdQuery>;
export type GetPricountByIdLazyQueryHookResult = ReturnType<typeof useGetPricountByIdLazyQuery>;
export type GetPricountByIdQueryResult = Apollo.QueryResult<GetPricountByIdQuery, GetPricountByIdQueryVariables>;
export const GetExpenseByIdDocument = gql`
    query getExpenseById($expenseId: Int!) {
  expense(id: $expenseId) {
    ...ExpenseFields
  }
}
    ${ExpenseFieldsFragmentDoc}`;

/**
 * __useGetExpenseByIdQuery__
 *
 * To run a query within a React component, call `useGetExpenseByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetExpenseByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetExpenseByIdQuery({
 *   variables: {
 *      expenseId: // value for 'expenseId'
 *   },
 * });
 */
export function useGetExpenseByIdQuery(baseOptions: Apollo.QueryHookOptions<GetExpenseByIdQuery, GetExpenseByIdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetExpenseByIdQuery, GetExpenseByIdQueryVariables>(GetExpenseByIdDocument, options);
      }
export function useGetExpenseByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetExpenseByIdQuery, GetExpenseByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetExpenseByIdQuery, GetExpenseByIdQueryVariables>(GetExpenseByIdDocument, options);
        }
export type GetExpenseByIdQueryHookResult = ReturnType<typeof useGetExpenseByIdQuery>;
export type GetExpenseByIdLazyQueryHookResult = ReturnType<typeof useGetExpenseByIdLazyQuery>;
export type GetExpenseByIdQueryResult = Apollo.QueryResult<GetExpenseByIdQuery, GetExpenseByIdQueryVariables>;
export const CreatePricountDocument = gql`
    mutation createPricount($pricountInput: CreatePricountInput!) {
  createPricount(createPricountInput: $pricountInput) {
    id
    title
    description
  }
}
    `;
export type CreatePricountMutationFn = Apollo.MutationFunction<CreatePricountMutation, CreatePricountMutationVariables>;

/**
 * __useCreatePricountMutation__
 *
 * To run a mutation, you first call `useCreatePricountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePricountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPricountMutation, { data, loading, error }] = useCreatePricountMutation({
 *   variables: {
 *      pricountInput: // value for 'pricountInput'
 *   },
 * });
 */
export function useCreatePricountMutation(baseOptions?: Apollo.MutationHookOptions<CreatePricountMutation, CreatePricountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreatePricountMutation, CreatePricountMutationVariables>(CreatePricountDocument, options);
      }
export type CreatePricountMutationHookResult = ReturnType<typeof useCreatePricountMutation>;
export type CreatePricountMutationResult = Apollo.MutationResult<CreatePricountMutation>;
export type CreatePricountMutationOptions = Apollo.BaseMutationOptions<CreatePricountMutation, CreatePricountMutationVariables>;
export const UpdatePricountDocument = gql`
    mutation updatePricount($updatePricountInput: UpdatePricountInput!) {
  updatePricount(updatePricountInput: $updatePricountInput) {
    ...PricountFields
  }
}
    ${PricountFieldsFragmentDoc}`;
export type UpdatePricountMutationFn = Apollo.MutationFunction<UpdatePricountMutation, UpdatePricountMutationVariables>;

/**
 * __useUpdatePricountMutation__
 *
 * To run a mutation, you first call `useUpdatePricountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePricountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePricountMutation, { data, loading, error }] = useUpdatePricountMutation({
 *   variables: {
 *      updatePricountInput: // value for 'updatePricountInput'
 *   },
 * });
 */
export function useUpdatePricountMutation(baseOptions?: Apollo.MutationHookOptions<UpdatePricountMutation, UpdatePricountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdatePricountMutation, UpdatePricountMutationVariables>(UpdatePricountDocument, options);
      }
export type UpdatePricountMutationHookResult = ReturnType<typeof useUpdatePricountMutation>;
export type UpdatePricountMutationResult = Apollo.MutationResult<UpdatePricountMutation>;
export type UpdatePricountMutationOptions = Apollo.BaseMutationOptions<UpdatePricountMutation, UpdatePricountMutationVariables>;
export const CreateExpenseDocument = gql`
    mutation createExpense($createExpenseInput: CreateExpenseInput!) {
  createExpense(createExpenseInput: $createExpenseInput) {
    ...ExpenseFields
  }
}
    ${ExpenseFieldsFragmentDoc}`;
export type CreateExpenseMutationFn = Apollo.MutationFunction<CreateExpenseMutation, CreateExpenseMutationVariables>;

/**
 * __useCreateExpenseMutation__
 *
 * To run a mutation, you first call `useCreateExpenseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateExpenseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createExpenseMutation, { data, loading, error }] = useCreateExpenseMutation({
 *   variables: {
 *      createExpenseInput: // value for 'createExpenseInput'
 *   },
 * });
 */
export function useCreateExpenseMutation(baseOptions?: Apollo.MutationHookOptions<CreateExpenseMutation, CreateExpenseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateExpenseMutation, CreateExpenseMutationVariables>(CreateExpenseDocument, options);
      }
export type CreateExpenseMutationHookResult = ReturnType<typeof useCreateExpenseMutation>;
export type CreateExpenseMutationResult = Apollo.MutationResult<CreateExpenseMutation>;
export type CreateExpenseMutationOptions = Apollo.BaseMutationOptions<CreateExpenseMutation, CreateExpenseMutationVariables>;
export const UpdateExpenseDocument = gql`
    mutation updateExpense($updateExpenseInput: UpdateExpenseInput!) {
  updateExpense(updateExpenseInput: $updateExpenseInput) {
    ...ExpenseFields
  }
}
    ${ExpenseFieldsFragmentDoc}`;
export type UpdateExpenseMutationFn = Apollo.MutationFunction<UpdateExpenseMutation, UpdateExpenseMutationVariables>;

/**
 * __useUpdateExpenseMutation__
 *
 * To run a mutation, you first call `useUpdateExpenseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateExpenseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateExpenseMutation, { data, loading, error }] = useUpdateExpenseMutation({
 *   variables: {
 *      updateExpenseInput: // value for 'updateExpenseInput'
 *   },
 * });
 */
export function useUpdateExpenseMutation(baseOptions?: Apollo.MutationHookOptions<UpdateExpenseMutation, UpdateExpenseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateExpenseMutation, UpdateExpenseMutationVariables>(UpdateExpenseDocument, options);
      }
export type UpdateExpenseMutationHookResult = ReturnType<typeof useUpdateExpenseMutation>;
export type UpdateExpenseMutationResult = Apollo.MutationResult<UpdateExpenseMutation>;
export type UpdateExpenseMutationOptions = Apollo.BaseMutationOptions<UpdateExpenseMutation, UpdateExpenseMutationVariables>;
export const AssignUserToParticipantDocument = gql`
    mutation assignUserToParticipant($id: Int!, $participantId: Int!) {
  assignUserToParticipant(id: $id, participantId: $participantId) {
    id
    participants {
      ...ParticipantInfo
    }
  }
}
    ${ParticipantInfoFragmentDoc}`;
export type AssignUserToParticipantMutationFn = Apollo.MutationFunction<AssignUserToParticipantMutation, AssignUserToParticipantMutationVariables>;

/**
 * __useAssignUserToParticipantMutation__
 *
 * To run a mutation, you first call `useAssignUserToParticipantMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAssignUserToParticipantMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [assignUserToParticipantMutation, { data, loading, error }] = useAssignUserToParticipantMutation({
 *   variables: {
 *      id: // value for 'id'
 *      participantId: // value for 'participantId'
 *   },
 * });
 */
export function useAssignUserToParticipantMutation(baseOptions?: Apollo.MutationHookOptions<AssignUserToParticipantMutation, AssignUserToParticipantMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AssignUserToParticipantMutation, AssignUserToParticipantMutationVariables>(AssignUserToParticipantDocument, options);
      }
export type AssignUserToParticipantMutationHookResult = ReturnType<typeof useAssignUserToParticipantMutation>;
export type AssignUserToParticipantMutationResult = Apollo.MutationResult<AssignUserToParticipantMutation>;
export type AssignUserToParticipantMutationOptions = Apollo.BaseMutationOptions<AssignUserToParticipantMutation, AssignUserToParticipantMutationVariables>;