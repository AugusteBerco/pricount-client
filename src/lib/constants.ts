import { ExpenseCategories, ExpenseTypes } from "generated-graphql";

export const EXPENSE_CATEGORIES = [
  {
    value: ExpenseCategories.Unset,
    label: "No Category",
  },
  {
    value: ExpenseCategories.Rent,
    label: "Rent & Charges",
  },
  {
    value: ExpenseCategories.Accomodation,
    label: "Accomodation",
  },
  {
    value: ExpenseCategories.Fun,
    label: "Entertainment",
  },
  {
    value: ExpenseCategories.Groceries,
    label: "Groceries",
  },
  {
    value: ExpenseCategories.Health,
    label: "Healthcare",
  },
  {
    value: ExpenseCategories.Insurance,
    label: "Insurance",
  },
  {
    value: ExpenseCategories.Restaurants,
    label: "Restaurant & Bars",
  },
  {
    value: ExpenseCategories.Shopping,
    label: "Shopping",
  },
  {
    value: ExpenseCategories.Transport,
    label: "Transport",
  },
];

export const EXPENSE_TYPES = [
  {
    value: ExpenseTypes.Expense,
    label: "Expense",
  },
  { value: ExpenseTypes.Transfer, label: "Money Transfer" },
];
