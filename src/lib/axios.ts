import axios from "axios";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    Authorization: `Bearer ${localStorage.getItem("token")}`,
  },
});

export const fetchNewToken = (originalReq?: any) => {
  return fetch(`${process.env.REACT_APP_API_URL}/auth/token`, {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    // headers: {
    //     'Content-Type': 'application/json',
    //     'Device': 'device',
    //     'Token': localStorage.getItem("token")
    // },
    redirect: "follow",
    referrer: "no-referrer",
    // body: JSON.stringify({
    //     token: localStorage.getItem("token"),
    //     refresh_token: localStorage.getItem("refresh_token")
    // }),
  })
    .then((res) => res.json())
    .then((res) => {
      localStorage.setItem("token", res.access_token);
      // originalReq.headers['Token'] = res.token;
      // originalReq.headers['Device'] = "device";

      return axios(originalReq);
    });
};

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (err) => {
    return new Promise((resolve, reject) => {
      console.log(err);
      const originalReq = err.config;
      if (
        err.response.status === 401 &&
        err.config &&
        !err.config.__isRetryRequest
      ) {
        originalReq._retry = true;

        let res = fetchNewToken(originalReq);

        resolve(res);
      }

      return Promise.reject(err);
    });
  }
);

export default axiosInstance;
