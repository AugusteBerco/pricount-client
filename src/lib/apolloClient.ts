import {
  ApolloClient,
  concat,
  createHttpLink,
  InMemoryCache,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";
import jwt from "jsonwebtoken";
import { toast } from "react-toastify";

const httpLink = createHttpLink({
  uri: `${process.env.REACT_APP_API_URL}/graphql`,
  credentials: "same-origin",
});

const errorLink = onError(
  ({ graphQLErrors, networkError, operation, forward }) => {
    if (graphQLErrors) {
      console.log(graphQLErrors);

      for (let err of graphQLErrors) {
        toast.error(err.message);
        //   switch (err.extensions?.code) {
        //     // Apollo Server sets code to UNAUTHENTICATED
        //     // when an AuthenticationError is thrown in a resolver
        //     case "INTERNAL_SERVER_ERROR":
        //       if (err.extensions.exception.response?.statusCode !== 401)
        //         return forward(operation);
        //       return fromPromise(
        //         fetch(`${process.env.REACT_APP_API_URL}/auth/token`, {
        //           method: "POST",
        //           mode: "cors",
        //           cache: "no-cache",
        //           credentials: "include",
        //           redirect: "follow",
        //           referrer: "no-referrer",
        //         }).then((res) => {
        //           return res.json();
        //         })
        //       )
        //         .filter((value) => Boolean(value))
        //         .flatMap((accessToken) => {
        //           // const token = accessToken.body.
        //           const oldHeaders = operation.getContext().headers;
        //           // modify the operation context with a new token
        //           operation.setContext({
        //             headers: {
        //               ...oldHeaders,
        //               authorization: `Bearer ${accessToken.access_token}`,
        //             },
        //           });
        //           localStorage.setItem("token", accessToken.access_token);
        //           // retry the request, returning the new observable
        //           return forward(operation);
        //         });
        //     // // Retry the request, returning the new observable
        //     // return forward(operation);
        //   }
      }
    }

    // To retry on network errors, we recommend the RetryLink
    // instead of the onError link. This just logs the error.
    if (networkError) {
      console.log(`[Network error]: ${networkError}`);
    }
  }
);

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem("token");

  const { exp } = jwt.decode(token!) as {
    exp: number;
  };
  const expirationDatetimeInSeconds = exp * 1000;

  // check if token is expired
  if (Date.now() >= expirationDatetimeInSeconds) {
    return new Promise(async (resolve, err) => {
      try {
        const newTokenBody = await fetch(
          `${process.env.REACT_APP_API_URL}/auth/token`,
          {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "include",
            redirect: "follow",
            referrer: "no-referrer",
          }
        );
        const newToken = await newTokenBody.json();
        localStorage.setItem("token", newToken.access_token);
        resolve({
          headers: {
            ...headers,
            authorization: token ? `Bearer ${newToken.access_token}` : "",
          },
        });
      } catch (e) {
        console.log(e);
        err(e);
      }
    });
  } else {
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : "",
      },
    };
  }
  // return the headers to the context so httpLink can read them
});

export const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(errorLink, authLink.concat(httpLink)),
});
